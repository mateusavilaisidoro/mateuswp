<?php 
error_reporting(0);
ini_set('display_errors', 0);
$get = file_get_contents('php://input');
$g = json_decode($get, true);
$spam = false;
// $return = array();
// $return['status_title'] = "Muito obrigado!";
// $return['status_text'] = "O e-mail foi enviado com sucesso! Logo entrarei em contato contigo.";
// $return['send'] = $g;
// echo json_encode($return);
// verificar se está limpo
if (preg_match( "/bcc:|cc:|multipart|\[url|Content-Type:/i", implode($g))) {
	$spam = true;  
}
if (preg_match_all("/<a|http:/i", implode($g), $out) > 3) {      
	$spam = true;  
}

$return = array();
if(!$spam){

	require("config/class.phpmailer.php");
	include("config/class.smtp.php");
	$mail = new PHPMailer();
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	// $mail->Host = "smtp.topofpestana.com";
	$mail->Host = 'smtp.gmail.com';  
	$mail->SMTPAuth = true; // Autenticação
	$mail->Username = 'mateus@mateusavila.com.br'; // Usuário do servidor SMTP
	$mail->Password = 'carol102030ab'; // Senha da caixa postal utilizada
	$mail->From = "mateus@mateusavila.com.br"; 
	$mail->FromName = "Webmaster";
	$mail->AddAddress('mateus@mateusavila.com.br', 'Webmaster');
	$mail->AddReplyTo($g['email'], $g['nome']);
	$mail->IsHTML(true);
	$mail->SMTPSecure = 'ssl'; 
	$mail->Port       = 465;
	$mail->CharSet = 'UTF-8';
	$mail->Subject  = $g['nome']." realizou um orçamento.";
	$mail->Body = "
	<h3>Orçamento realizado:</h3>
	<p><strong>".$g['nome']."</strong>, com o e-mail <strong><a href=\"mailto:".$g['email']."\">".$g['email']."</a></strong> e telefone <strong>".$g['telefone']."</strong> realizou um orçamento:</p>
	<table class=\"table\" style=\"width: 600px; border: 1px solid #ddd; font-size: 13px;\">
		<thead>
			<tr style=\"background: #34495e; color: #fff\">
				<th style=\"text-align: left; padding: 5px; width: 50%;\">Item</th>
				<th style=\"text-align: right; padding: 5px; width: 50%;\">Valor</th>
			</tr>
		</thead>
		<tbody>
			<tr style=\"border: 1px solid #ddd; background: #fff;\">
				<td style=\"padding: 5px;\">Tipo de Projeto</td>
				<td style=\"text-align: right;padding:5px;\">".$g['type']."</td>
			</tr>
			<tr style=\"border: 1px solid #ddd; background: #f0f0f0;\">
				<td style=\"padding: 5px;\">Qtd. páginas/sessões</td>
				<td style=\"text-align: right;padding:5px;\">".$g['pages']."</td>
			</tr>
			<tr style=\"border: 1px solid #ddd; background: #fff;\">
				<td style=\"padding: 5px;\">Tipo de Desenvolvimento:</td>
				<td style=\"text-align: right;padding:5px;\">".($g['hurry'] ? 'Expresso' : 'Padrão')."</td>
			</tr>
			<tr style=\"border: 1px solid #ddd; background: #f0f0f0;\">
				<td style=\"padding: 5px;\">Previsão de dias <br><small>(paginas/sessões * 2 / ".($g['hurry'] ? '8' : '4').")</small></td>
				<td style=\"text-align: right;padding:5px;\">".$g['dias']."</td>
			</tr>
			<tr style=\"border: 1px solid #ddd; background: #fff;\">
				<td style=\"padding: 5px;\">Previsão de horas <br><small>(paginas/sessões * 2)</small></td>
				<td style=\"text-align: right;padding:5px;\">".$g['horas']." horas</td>
			</tr>
			<tr style=\"border: 1px solid #ddd; background: #f0f0f0;\">
				<td style=\"padding: 5px;\">Hora técnica</td>
				<td style=\"text-align: right;padding:5px;\">".$g['horatecnicafinal']."</td>
			</tr>
		</tbody>
		<tfoot>
			<tr style=\"border: 1px solid #ddd; background:#34495e; color: #fff; font-size:15px; font-weight: bold;\">
				<td style=\"padding: 5px;\">Total</td>
				<td style=\"padding: 5px;text-align: right;\">R$ ".$g['total']."</td>
			</tr>
		</tfoot>
	</table>
	<p>Como observações, ele colocou:</p>
	<p>".$g['mensagem']."</p>
	";
	$enviado = $mail->Send();
	
	$mail->debug = false;
	if ($enviado) {
		$return['status_title'] = "Muito obrigado!";
		$return['status_text'] = "Seu orçamento foi enviado com sucesso! Logo logo retornarei o contato!";
		echo json_encode($return);
	} else {
		$return['status_title'] = "Deu Erro!";
		$return['status_text'] = $mail->ErrorInfo;
		echo json_encode($return);
	}
}else{
	$return['status'] = 0;
	$return['result'] = "Filtro anti-spam";
	echo json_encode($return);
}

