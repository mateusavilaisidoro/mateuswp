<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js home-html"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Mateus Ávila - Desenvolvedor web</title>
	<meta name="description" content="Freelancer em desenvolvimento web. Focado em projetos responsivos, utilizando as tecnologias mais avançadas disponíveis. Criação de temas para Wordpress, recorte de PSD para HTML/CSS, aplicação de APIs externas, hotsites, sites institucionais.">
	<meta name="keywords" content="Desenvolvimento Web, Wordpress, Layouts responsivos, Bento Gonçalves, Mobile, Javascript, Aplicativos para Facebook, HTML 5, CSS 3, Angular JS, jQuery, APIs externas, API do Youtube, API do Google Maps, API do Facebook, Stylus, otimização e performance web">
	<meta name="author" content="Mateus Ávila Isidoro"> 
	<meta name="viewport" content="width=device-width">
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/style.css">
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/img/favicon4.svg">

	<link rel="canonical" href="http://<?php echo $_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:title" content="Mateus Ávila - Desenvolvedor web" />
	<meta property="fb:app_id" content="218867438180293" />
	<meta property="og:description" content="Freelancer em desenvolvimento web. Focado em projetos responsivos, utilizando as tecnologias mais avançadas disponíveis. Criação de temas para Wordpress, recorte de PSD para HTML/CSS, aplicação de APIs externas, hotsites, sites institucionais." />
	<meta property="og:url" content="http://<?php echo $_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']; ?>img/share.jpg" />
</head>