<?php get_header(); ?>
<body>
	<div id="app">
		<header v-bind:class="{'active': portfolio}">
			<div class="logo">Mateus Ávila</div>
			<div class="center">
				<?php 
				$loop = new WP_Query( array('pagename' => 'home'));
				while ( $loop->have_posts() ) : $loop->the_post();
				?>
				<?php the_content(); ?>
				<br><br><p><a href="#" class="openContacts" v-on:click.prevent="change_expertise">Quem sou</a><a href="#" class="openContacts" v-on:click.prevent="change_portfolio">Portfólio</a><a href="#" class="openContacts" v-on:click.prevent="change_formmail">Contatos</a><a href="#" class="openContacts" v-on:click.prevent="change_orcamento">Orçamento</a></p>
				<?php 
				endwhile;
				wp_reset_query();
				?>
			</div>
		</header>
		<main v-bind:class="{'active': portfolio}">
			<span class="close" v-on:click="change_portfolio">&times;</span>
			<ul class="lista-projetos">
				<li v-for="i in lista" v-on:click.prevent="open_project(i.idcount)" v-bind:class="i.custom_fields.class[0]">
					<div class="bg-foto" v-bind:style="{ backgroundImage: 'url(' + i.bg + ')' }"></div>
					<span class="bg"></span>
					<span class="title">{{i.title}}</span>
				</li>
				<li class="disponivel"  v-on:click.prevent="change_formmail">
					<span class="bg"></span>
					<span class="title"><strong>Gostou?</strong> Estou disponível para o seu projeto</span>
				</li>
			</ul>
		</main>
		<div class="mask" v-on:click.prevent="close_mask" v-bind:class="{'active': view_projeto || formmail || expertise || orcamento}"></div>
		<div class="view-project right" v-bind:class="{'active': orcamento}">
			<span class="close2" v-on:click.prevent="close_mask">&times;</span>
			<?php 
			$loop = new WP_Query( array('pagename' => 'orcamento'));
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
			<p><strong><?php the_field('titulo_orcamento'); ?></strong></p>
			<div class="box">
				<?php $orc = get_field('orcamento'); ?>
				<div class="tipo-projeto">
					<?php 
					$size = sizeof($orc);
					for ($i=0; $i < $size; $i++) { 
						echo "<span v-on:click=\"defineType('".$orc[$i]['tipo']."', 40)\" v-bind:class=\"{'active': budget.type == '".$orc[$i]['tipo']."'}\">".$orc[$i]['tipo']." <br><small>(R$ ".$orc[$i]['valor'].",00)</small></span>";
					}
					?>
				</div>
				<?php 
				for ($i=0; $i < $size; $i++) { 
					echo "<div class=\"explain\" v-if=\"budget.type == '".$orc[$i]['tipo']."'\">
						".$orc[$i]['descricao']."
					</div>";
				}
				?>
			</div>
			<div class="box" v-if="budget.horatecnica > 0">
				<p><strong>Quantos telas diferentes (PSDs) o seu projeto possui?</strong></p>
				<div class="box-range">
					<input type="range" name="quantidade" id="quantidade" v-model="budget.pages" min="1" max="<?php the_field('telas'); ?>" step="1">
					<output for="quantidade" class="output">{{budget.pages}}</output>
				</div>
			</div>
			<div class="box" v-if="budget.horatecnica > 0">
				<?php the_field('texto_expresso'); ?>
				<div class="box-hurry">
					<span v-on:click="defineHurry(<?php the_field('decimal'); ?>)" v-bind:class="{'active': budget.hurry == '<?php the_field('decimal'); ?>'}" class="express">Expresso (+ <?php the_field('porcentagem'); ?>)</span>
					<span v-on:click="defineHurry(0)" v-bind:class="{'active': budget.hurry == '0'}" class="default">Padrão</span>
				</div>
			</div>
			<div class="box" v-if="budget.horatecnica > 0">
				<p><strong>Resumo do orçamento</strong></p>
				<table class="table">
					<thead>
						<tr>
							<th>Item</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Tipo de Projeto</td>
							<td>{{budget.type}}</td>
						</tr>
						<tr>
							<td>Qtd. páginas/sessões</td>
							<td>{{budget.pages}}</td>
						</tr>
						<tr>
							<td>Tipo de Desenvolvimento:</td>
							<td>{{budget.hurry ? 'Expresso' : 'Padrão'}}</td>
						</tr>
						<tr>
							<td>Previsão de dias <br><small>(paginas/sessões * 2 / {{budget.hurry ? '8' : '4'}})</small></td>
							<td>{{previsaoDias()}}</td>
						</tr>
						<tr>
							<td>Previsão de horas <br><small>(paginas/sessões * 2)</small></td>
							<td>{{previsaoHoras()}} horas</td>
						</tr>
						<tr>
							<td>Hora técnica</td>
							<td>{{totalhora()}}</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>Total</td>
							<td>R$ {{valortotal()}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="box"  v-if="budget.horatecnica > 0">
				<h2><?php the_field('titulo_formulario'); ?></h2>
				<?php the_field('texto_formulario'); ?>
				<div class="contatos-form">
					<div class="form-contact-loading" v-bind:class="{'active': formbudgetisloading == true}">
						<img src="<?php bloginfo('template_url') ?>/img/loading.svg" alt="">
					</div>
					<div class="form-contact-result" v-bind:class="{'active': formbudgetresult == true}">
						<h4>{{returnformbudget.status_title}}</h4>
						<p>{{returnformbudget.status_text}}</p>
						<span class="form-contact-close" v-on:click.prevent="formbudgetresult = false">&times; fechar</span>
					</div>
					<div class="form-contact-form">
						<form action="#" method="post" v-on:submit.prevent="budgetformsend()" name="budget" id="form">
							<label for="nome">Nome</label>
							<input type="text" name="nome" required="true" v-model="budget.nome" id="nome" placeholder="Ex: Mateus Ávila Isidoro">
							<label for="email">Email</label>
							<input type="email" name="email" required="true" v-model="budget.email" id="email" placeholder="Ex: mateus@mateusavila.com.br">
							<label for="telefone">Telefone</label>
							<input type="tel" name="telefone" required="true" v-model="budget.telefone" id="telefone" placeholder="Ex: (99) 99999 9999" maxlength="16">
							<label for="mensagem">Mensagem</label>
							<textarea name="mensagem" required="true" v-model="budget.mensagem" id="mensagem" placeholder="Deixe seu recado aqui"></textarea>
							<button type="submit" class="btn">Entrar em contato</button>
						</form>
					</div>
				</div>
			</div>
			<?php 
			endwhile;
			wp_reset_query();
			?>
		</div>
		<div class="view-project right" v-bind:class="{'active': view_projeto}">
			<span class="close2" v-on:click.prevent="close_mask">&times;</span>
			<h2>{{projeto_ativo.title}}</h2>
			<div class="view-project-img" v-bind:class="{'icon': projeto_ativo.custom_fields.bgclass[0] == '2' }">
				<div class="view-project-img-src" v-bind:class="{'active': projeto_ativo.custom_fields.bgclass[0] == 1, 'icon': projeto_ativo.custom_fields.bgclass[0] == '2' }" v-bind:style="{backgroundImage: 'url('+ projeto_ativo.bg +')'}"></div>
			</div>
			<div class="view-project-buttons">
				<span class="arrow arrow-prev" v-on:click.prevent="changeproject(projeto_ativo.prev)"></span>
				<span class="arrow arrow-next" v-on:click.prevent="changeproject(projeto_ativo.next)"></span>
				<a v-bind:href="projeto_ativo.custom_fields.url[0]" target="_blank" class="btn">Acessar o projeto</a>
			</div>
			<div class="text-content" v-html="projeto_ativo.content"></div>
			<hr>
			<p><strong>Agência:</strong> {{projeto_ativo.custom_fields.agency[0]}}</p>
			<p><strong>Cliente:</strong> {{projeto_ativo.custom_fields.client[0]}}</p>
			<p><strong>Tags:</strong> {{projeto_ativo.custom_fields.devtags[0]}}</p>
		</div>
		<div class="view-project left" v-bind:class="{'active': expertise}">
			<span class="close2" v-on:click.prevent="close_mask">&times;</span>
			<?php 
			$loop = new WP_Query( array('pagename' => 'quem-sou'));
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<h2><?php the_title(); ?></h2>
			<div class="mateus" style="background-image: url('<?php the_post_thumbnail_url()?>')"></div>
			<?php the_content(); ?>
			<?php 
			endwhile;
			wp_reset_query();
			?>
		</div>
		<div class="view-project left" v-bind:class="{'active': formmail}">
			<span class="close2" v-on:click.prevent="close_mask">&times;</span>
			<?php 
			$loop = new WP_Query( array('pagename' => 'entre-em-contato'));
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
			<?php 
			endwhile;
			wp_reset_query();
			?>
			<div class="contatos-form">
				<div class="form-contact-loading" v-bind:class="{'active': formcontatoisloading == true}">
					<img src="<?php bloginfo('template_url') ?>/img/loading.svg" alt="">
				</div>
				<div class="form-contact-result" v-bind:class="{'active': formcontatoresult == true}">
					<h4>{{returnform.status_title}}</h4>
					<p>{{returnform.status_text}}</p>
					<span class="form-contact-close" v-on:click.prevent="formcontatoresult = false">&times; fechar</span>
				</div>
				<div class="form-contact-form">
					<form action="#" method="post" v-on:submit.prevent="contactform()" name="contato" id="form">
						<label for="nome">Nome</label>
						<input type="text" name="nome" required="true" v-model="form.nome" id="nome" placeholder="Ex: Mateus Ávila Isidoro">
						<label for="email">Email</label>
						<input type="email" name="email" required="true" v-model="form.email" id="email" placeholder="Ex: mateus@mateusavila.com.br">
						<label for="telefone">Telefone</label>
						<input type="tel" name="telefone" required="true" v-model="form.telefone" id="telefone" placeholder="Ex: (99) 99999 9999" maxlength="16">
						<label for="mensagem">Mensagem</label>
						<textarea name="mensagem" required="true" v-model="form.mensagem" id="mensagem" placeholder="Deixe seu recado aqui"></textarea>
						<button type="submit" class="btn">Entrar em contato</button>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>