var view = new Vue({
	el: '#app',
	data: function(){
		return{
			lista: [],
			view_projeto: false,
			portfolio: false,
			formmail: false,
			expertise: false,
			orcamento: false,
			orcamento_contato: false,
			quantidade: 0,
			projeto_ativo: {
				custom_fields:{
					bgclass: '',
					description: '',
					url: '',
					devtags: '',
					client: '', 
					agency: ''
				}
			},
			budget: {
				horatecnica: 0,
				type: '',
				pages: 1,
				hurry: 0,
				nome: '',
				email: '',
				telefone: '',
				mensagem: '',
				dias: 1,
				horas: 2,
				total: 0
			},
			form: {
				nome: '',
				email: '',
				telefone: '',
				mensagem: ''
			},
			returnform: {
				status_title: '',
				status_text: ''
			},
			returnformbudget:{
				status_title: '',
				status_text: ''
			},
			formcontatoresult: false,
			formcontatoisloading: false,
			formbudgetresult: false,
			formbudgetisloading: false
		};
	}, 
	created: function(){
		this.listagem();
	},
	methods: {
		listagem: function(){
			this.$http.get('api/projetos/get_posts').then(function(response){
				this.lista = response.body.posts;
				this.quantidade = this.lista.length;
			});
		},
		valortotal: function(){
			this.budget.total = (this.previsaoHoras() * this.totalhora()).toFixed(2);
			return this.budget.total;
		},
		previsaoHoras: function(){
			this.budget.horas = this.budget.pages * 2;
			return this.budget.horas;
		},
		previsaoDias: function(){
			this.budget.dias = this.budget.hurry ? Math.ceil(this.budget.pages * 2 / 8) : Math.ceil(this.budget.pages * 2 / 4);
			return this.budget.dias;
		},
		totalhora: function(){
			this.budget.horatecnicafinal = this.budget.hurry ? (this.budget.horatecnica + (this.budget.horatecnica * this.budget.hurry)).toFixed(2) : this.budget.horatecnica.toFixed(2);
			return this.budget.horatecnicafinal;
		},
		defineType: function(type, hour){
			this.budget.type = type;
			this.budget.horatecnica = hour;
		},
		defineHurry: function(hurry){
			this.budget.hurry = hurry;
		},
		
		change_portfolio: function(){
			this.portfolio = !this.portfolio;
		},
		change_orcamento: function(){
			this.orcamento = !this.orcamento;
		},
		change_expertise: function(){
			this.expertise = !this.expertise;
		},
		change_formmail: function(){
			this.formmail = !this.formmail;
		},
		changeproject: function(id){
			this.projeto_ativo = this.lista[id];
			this.projeto_ativo.next = ((this.quantidade - 1) === id) ? 0 : (id + 1);
			this.projeto_ativo.prev = (id === 0) ? (this.quantidade - 1) : ( id - 1);
		},
		open_project: function(id){
			this.view_projeto = true;
			this.changeproject(id);
		},
		close_mask: function(){
			this.view_projeto = false;
			this.formmail = false;
			this.expertise = false;
			this.orcamento = false;
			this.projeto_ativo.icon = false;
			this.projeto_ativo.bgclass = false;
		},
		contactform: function(){
			this.formcontatoisloading = true;
			this.$http.post('wp-content/themes/mateus/send.php', this.form).then(function(response){
				this.returnform = JSON.parse(response.body);
				this.form = {};
				this.formcontatoresult = true;
				this.formcontatoisloading = false;
			});
		},
		budgetformsend: function(){
			this.formbudgetisloading = true;
			// console.log(this.budget);
			this.$http.post('wp-content/themes/mateus/send-budget.php', this.budget).then(function(response){
				this.returnformbudget = JSON.parse(response.body);
				this.budget.nome = '';
				this.budget.email = '';
				this.budget.telefone = '';
				this.budget.mensagem = '';
				this.formbudgetresult = true;
				this.formbudgetisloading = false;
			});
		}
	}
});