<?php
// 1. The class name must match the filename (i.e., "foo.php" for JSON_API_Foo_Controller)
// 2. Save this in your themes folder
// 3. Activate your controller under Settings > JSON API
class JSON_API_Projetos_Controller {
  
  public function get_posts() {
    global $json_api;
    global $post;
    
    // if (empty($_GET['meta_query'])) {
    //   return array(
    //     'error' => "Specify a 'meta_query' param."
    //   );
    // }
    
    /*
    
    Pass meta_query as URL-encoded JSON
    To borrow the example on http://codex.wordpress.org/Class_Reference/WP_Query
    
    $args = array(
      'meta_query' => array(
        array(
          'key' => 'age',
          'value' => array(3, 4),
          'compare' => 'IN',
        )
      )
    );
    
    Here's how that would look as a URL query param:
    /?json=example/get_posts_by_meta_query&meta_query=%5B%7B%22key%22%3A%22age%22%2C%22value%22%3A%5B3%2C4%5D%2C%22compare%22%3A%22in%22%7D%5D
    
    In JavaScript:
    var meta_query = encodeURIComponent(
      JSON.stringify([
        {
          key: 'age',
          value: [3, 4],
          compare: 'in'
        }
      ])
    );
    
    In PHP:
    $meta_query = rawurlencode(
      json_encode(array(
        'key' => 'age',
        'value' => array(3, 4),
        'compare' => 'IN',
      ))
    );
    
    */
    
    $query = array(
      'ignore_sticky_posts' => true,
      'post_type'=>'projetos',
      'order'=>'ASC',
      'json'=>'projetos',
      'orderby'=>'menu_order',
      'posts_per_page'=>-1
    );
    $posts = $json_api->introspector->get_posts($query);
    // var_dump($posts);
    $size = sizeof($posts);
    $size2 = sizeof($posts[0]->attachments);

    // loop para identificar as imagens
    for ($i=0; $i < $size; $i++) { 
      $posts[$i]->idcount = $i;
      for ($j=0; $j < $size2; $j++) { 
        if($posts[$i]->custom_fields->bg[0] == $posts[0]->attachments[$j]->id){
          $posts[$i]->bg = $posts[0]->attachments[$j]->url;
        }
      }
      
    }
    return array(
      'posts' => $posts,
      'contagem'=> $size
    );
  }
}
?>